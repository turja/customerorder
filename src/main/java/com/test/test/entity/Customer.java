package com.test.test.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
@Getter
@Setter
@Entity
@Table(name = "das_customer")
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "customerID")
    private int customerID;

    @Column(name = "customerName")
    private String customerName;

    @Column(name = "customerAge")
    private int customerAge;

    @Column(name = "customerAddress")
    private String customerAddress;
}
