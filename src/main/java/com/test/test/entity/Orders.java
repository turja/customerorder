package com.test.test.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "das_orders")
@Getter
@Setter
public class Orders {
    @Id
    @GeneratedValue
    @Column(name = "orderId")
    private long orderId;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name="customerID")
    @OrderBy("customerName")
    private Customer customer;

    @Column(name = "orderDetail")
    private String orderDetail;

    @Column(name = "orderDate")
    private Date orderDate;

    @Column(name = "orderAmount")
    private int orderAmount;
}
