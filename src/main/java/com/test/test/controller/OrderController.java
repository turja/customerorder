package com.test.test.controller;

import com.test.test.api.OrderResponse;
import com.test.test.entity.Customer;
import com.test.test.entity.Orders;
import com.test.test.repository.CustomerRepository;
import com.test.test.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/orders")
public class OrderController {
    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private OrderRepository orderRepository;

    @PostMapping("/{customerId}/orders")
    public void saveOrders(@PathVariable(value = "customerId") Integer customerId,
                                   @RequestBody Orders orders) {
        Customer customer = customerRepository.findById(customerId).get();
        if (customer != null) {
            orders.setCustomer(customer);
            orderRepository.save(orders);
        }
//        customerRepository.findById(customerId).map(customer -> {
//            orders.setCustomer(customer);
//            return orderRepository.save(orders);
//        });
    }

    @GetMapping(value = "/{customerId}/orders", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<OrderResponse> getOrders(@PathVariable(value = "customerId") Integer customerId) {
        List<Orders> orderList = orderRepository.findByCustomerCustomerID(customerId);
        return buildResponse(orderList);
    }

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<OrderResponse> getAllOrders() {
        List<Orders> orderList = orderRepository.findAll();
        return buildResponse(orderList);
    }

    private List<OrderResponse> buildResponse(List<Orders> orderList) {
        return orderList.stream().map(order ->
                OrderResponse.builder().customerID(order.getCustomer().getCustomerID())
                        .customerName(order.getCustomer().getCustomerName())
                        .customerAge(order.getCustomer().getCustomerAge())
                        .customerAddress(order.getCustomer().getCustomerAddress())
                        .orderID(order.getOrderId())
                        .orderDetail(order.getOrderDetail())
                        .orderDate(order.getOrderDate())
                        .orderAmount(order.getOrderAmount())
                        .build()).collect(Collectors.toList());
    }

    public int getWordCounts(String sentence) {
        int count = 0;
        if (sentence == null || sentence.length() == 0) return 0;

        String[] words = sentence.split("\\s+|,\\s*|\\.\\s*");
        return words.length;
    }
}
