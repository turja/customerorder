package com.test.test.controller;

import com.test.test.entity.Customer;
import com.test.test.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.awt.*;
import java.util.List;

@RestController
@RequestMapping
public class CustomerController {
    @Autowired
    private CustomerRepository customerRepository;

    @GetMapping(value = "/customers", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Customer> getCustomers() {
        List<Customer> lst =  customerRepository.findAll();
        return lst;
    }

    @PostMapping("/customers")
    public void addCustomer(@RequestBody Customer customer) {
        customerRepository.save(customer);
    }
}
