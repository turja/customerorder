package com.test.test.api;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.Date;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class OrderResponse {
    private int customerID;
    private String customerName;
    private int customerAge;
    private String customerAddress;

    private long orderID;
    private String orderDetail;
    private Date orderDate;
    private int orderAmount;
}
